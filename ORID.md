O:

Today, I first code reviewed yesterday's homework, and then the teacher explained the reasons and methods of useEffect, etc. Then we carried out the development of front and back end pull through, and wrote API for yesterday's project by ourselves. The afternoon went on to show some code implementations and cross-domain issues with front - and back-end access, and finally a presentation for each group.

R:

What I learned today made me feel very full. After more than two weeks of front-to-back learning, I completely completed the front-to-back interaction with what I learned.

I:

Our group did not perform very well in today's presentation. The other two groups and the teachers also gave a lot of suggestions. We will also improve on the suggestions of the teachers in the future.

D:

The front-end training has almost come to an end, but the learning is far more than what the teacher said in class, and I will also do some independent learning in the future.