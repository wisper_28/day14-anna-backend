package com.example.todoList;

import com.example.todoList.entity.Todo;
import com.example.todoList.repository.JPATodoRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.Random.class)
public class TodoController {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPATodoRepository jpaTodoRepository;

    @BeforeEach
    void setup() {
        jpaTodoRepository.deleteAll();
    }


    @Test
    void should_return_all_todos_when_find_all_todos_given_() throws Exception {
        Todo todo1 = new Todo(null, "111", false);
        Todo todo2 = new Todo(null, "222", false);
        jpaTodoRepository.save(todo1);
        jpaTodoRepository.save(todo2);
        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo1.isDone()));
    }
    @Test
    void should_return_todo_when_find_by_id_given_id() throws Exception {
        //given
        Todo todo = new Todo(null, "111", false);
        jpaTodoRepository.save(todo);
        //when
        mockMvc.perform(get("/todos/{id}", todo.getId()))
               .andExpect(MockMvcResultMatchers.status().is(200))
               .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
               .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
               .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    void should_delete_todo_when_delete_todo_given_id() throws Exception {
        //given
        Todo todo = new Todo(null, "111", false);
        jpaTodoRepository.save(todo);
        //when
        mockMvc.perform(delete("/todos/{id}", todo.getId()))
              .andExpect(MockMvcResultMatchers.status().is(204));
        //then
        assertTrue(jpaTodoRepository.findById(todo.getId()).isEmpty());
    }

    @Test
    void should_create_todo_when_create_todo_given_todo() throws Exception {
        //given
        Todo todo = new Todo(null, "111", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJSON = objectMapper.writeValueAsString(todo);
        //when
        mockMvc.perform(post("/todos")
              .contentType(MediaType.APPLICATION_JSON)
              .content(todoRequestJSON))
              .andExpect(MockMvcResultMatchers.status().is(201))
              .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
              .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
              .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.isDone()));
    }

    @Test
    void should_update_todo_when_update_given_todo() throws Exception {
        //given
        Todo todo = new Todo(null, "111", false);
        jpaTodoRepository.save(todo);
        Todo todoUpdated = new Todo(todo.getId(), "222", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJSON = objectMapper.writeValueAsString(todoUpdated);
        //when
        mockMvc.perform(put("/todos/{id}", todo.getId())
             .contentType(MediaType.APPLICATION_JSON)
             .content(todoRequestJSON))
             .andExpect(MockMvcResultMatchers.status().is(200))
             .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
             .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todoUpdated.getText()))
             .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoUpdated.isDone()));

        //when

        //then
    }
}
