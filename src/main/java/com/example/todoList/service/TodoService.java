package com.example.todoList.service;

import com.example.todoList.entity.Todo;
import com.example.todoList.exception.TodoNotFoundException;
import com.example.todoList.repository.JPATodoRepository;
import com.example.todoList.service.dto.TodoRequest;
import com.example.todoList.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    private final JPATodoRepository jpaTodoRepository;

    public TodoService(JPATodoRepository jpaTodoRepository) {
        this.jpaTodoRepository = jpaTodoRepository;
    }

    public List<Todo> getAllTodos() {
        return jpaTodoRepository.findAll();
    }

    public Todo createTodo(TodoRequest todoRequest) {
        Todo todo = TodoMapper.toEntity(todoRequest);
        return jpaTodoRepository.save(todo);
    }

    public void deleteTodo(Long id) {
        jpaTodoRepository.deleteById(id);
    }

    public Todo updateTodo(Long id, Todo todo) {
        Todo preTodo = jpaTodoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
        if (todo.getText() != null) {
            preTodo.setText(todo.getText());
        }
        preTodo.setDone(todo.isDone());
        return jpaTodoRepository.save(preTodo);
    }

    public Todo getTodo(Long id) {
        return jpaTodoRepository.findById(id).orElseThrow(TodoNotFoundException::new);
    }
}
