package com.example.todoList.service.mapper;

import com.example.todoList.entity.Todo;
import com.example.todoList.service.dto.TodoRequest;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest, todo);
        todo.setDone(false);
        return todo;
    }
}
