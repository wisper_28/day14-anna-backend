package com.example.todoList.repository;

import com.example.todoList.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JPATodoRepository extends JpaRepository<Todo, Long> {
}
